import time
from umqttsimple import MQTTClient
from ubinascii import hexlify
import machine
import network
from machine import Pin,I2C
import BME280 as bme280
#garbage collection
from gc import collect
collect()

from ntptime import settime
from json import dumps

mqtt_server = '20.91.211.91'
#EXAMPLE IP ADDRESS or DOMAIN NAME
#mqtt_server = '192.168.1.106'

client_id = hexlify(machine.unique_id())

topic_1 = b'rpi1/number'

last_message = 0
message_interval = 50

i2c = I2C(0,scl=Pin(22),sda=Pin(21))
bme = bme280.BME280(i2c=i2c, address=0x77)

led = machine.Pin(2, machine.Pin.OUT)

station = network.WLAN(network.STA_IF)

device_id = -1
DEVICE_ID_FILE = 'deviceID.txt'
with open(DEVICE_ID_FILE) as f:
    device_id = int(f.readline())

def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server,keepalive=10)
  #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(1)
  machine.reset()

def build_payload(sensor_reading, device_id):
    timestamp = round((time.time() + 946684800)*1000)  # unix time Epoch of 1970 in milliseconds
    payload = {'sensor_id': device_id, 'UTC': timestamp, 't': round(sensor_reading[0],2),'h':round(sensor_reading[1],2),'p':round(sensor_reading[2])}
    return dumps(payload) # convert to json format

def read_sensor():
  try:
    temperature, pressure, humidity = bme.read_compensated_data()
    temperature = temperature/100
    pressure = pressure / 256
    humidity = humidity / 1024
    if (isinstance(temperature, float) and isinstance(humidity, float) and isinstance(pressure, float)) or (isinstance(temperature, int) and isinstance(humidity, int), isinstance(pressure, int)):
      return temperature, humidity, pressure
    else:
      return('Invalid sensor readings.')
  except OSError as e:
    return('Failed to read sensor.')

def box_filter():
    temperature_list=[]
    humidity_list=[]
    pressure_list=[]
    for _ in range(10):
        t,h,p = read_sensor()
        temperature_list.append(t)
        humidity_list.append(h)
        pressure_list.append(p)
        time.sleep(0.1)
    temperature_filtered=sum(temperature_list)/len(temperature_list)
    humidity_filtered=sum(humidity_list)/len(humidity_list)
    pressure_filtered=sum(pressure_list)/len(pressure_list)
    return temperature_filtered, humidity_filtered, pressure_filtered

def led_blink():
    led.value(0.5)
    time.sleep(0.5)
    led.value(0)

try:
    settime()
except:
    machine.reset()

while True:
  try:
      client = connect_mqtt()
      payload = build_payload(read_sensor(), device_id)
      print(f'Publishing {payload} to topic, {topic_1}')
      client.publish(topic_1, payload)
      led_blink()
      time.sleep(1)
      machine.deepsleep(message_interval*1000)
  except OSError as e:
    restart_and_reconnect()
