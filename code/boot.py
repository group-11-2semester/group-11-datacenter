# This file is executed on every boot (including wake-boot from deepsleep)
# import esp
# esp.osdebug(None)
# import webrepl
# webrepl.start()
import wifimgr
import machine
import time

import _thread
#reset
from esp32 import wake_on_ext0, WAKEUP_ALL_LOW
from os import remove
button = machine.Pin(0,machine.Pin.IN)

#deepsleep interupt
wake_on_ext0(pin = button, level = WAKEUP_ALL_LOW)

# to reset press EN then press BOOT
if button.value() == 0:
  time.sleep(3)
  if button.value() == 0:
      try:
          remove("deviceID.txt")
          remove("wifi.dat")
      except OSError:
          print("No files")

#if led is constant on setup of network is required.
#led on
blink = True
led = machine.Pin(2, machine.Pin.OUT)
led.value(1)
try:
    wlan = wifimgr.get_connection()
except:
    machine.reset()
if wlan is None:
    print("Could not initialize the network connection.")
    time.sleep(1)
    machine.reset()
#led off
led.value(0)


import HTP_mqtt.py