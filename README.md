# Group 11 Datacenter
Group 11 2. semester project

## Project topics
### Only the ingoing and outgoing temp in known
![server racks](img/Racks.jpg)

- Pressure 
	- The presure should be higher on the cold side about 4 pa
- Temperature - The temperature might be different in the different racks
	- Sense the temperature 
	- map
- Humidity - The humidity is important 
	- sense the humidity
	- map
- Load on each phase 
	- magnetic field
- Security
	- chip lock
	- CCTV
- Water in the raised floor.